Writer's Guide

As a developer who used to be a content manager and who has trained content
managers for government and higher education, I've noticed that almost all
of our documentation is aimed at either site builders, developers, or the
do-it-yourself business owners. Not very much is aimed at content writers
and editors who don't have a hand in creation or development of the site.

This module is for them, it consists of tours for the major (unchanging)
aspects of Drupal 8/9 sites and is meant as somewhat of a quick reference
for them. The tours are short--they contain links to articles and/or video
tutorials that shed more light on the subject. The idea is to give
writers/editors the right terminology and understanding so they have the
ability to ask the right questions and make their business requirements clear.

These tours do not assume any knowledge of web development or intent to build
sites by the audience. Simply to give enough information to a user so they can
find the answers to their questions going forward.

Some of the links do go to the User's Guide when it seems appropriate. Other
times, they are linking out to articles that cover the main idea behind things.
For example, "content chunking" is not specific to Drupal and it can help
explain why we have fields and displays rather than a single body with
drag-and-drop functionality.
